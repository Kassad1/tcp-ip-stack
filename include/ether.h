#include "systemlib.h"

#define ETH_ALEN     6 /* Address Length */
#define ETH_HEADER_SIZE sizeof(struct ether)

struct eth_frame {
  uint8_t mac_dst[ETH_ALEN];
  uint8_t mac_src[ETH_ALEN];
  uint16_t type;
  uint8_t payload[];
} __attribute__((packed));
