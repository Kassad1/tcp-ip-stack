/* Double linked list */
struct node {
  struct node *prev, *next;
};

static inline void node_init(struct node *head)
{
  head->prev = head->next = head;
}

static inline void node_add(struct node *new, struct node *head)
{
  head->next->prev = new;
  new->next = head->next;
  new->prev = head;
  head->next = new;
}

static inline void node_add_tail(struct node *new,
				 struct node *head)
{

  head->prev->next = new;
  new->prev = head->prev;
  new->next = head;
  head->prev = new;
}

static inline void node_remove(struct node *x)
{
  struct node *prev = x->prev;
  struct node *next = x->next;

  prev->next = next;
  next->prev = prev;
}

