#include "list.h"


#define ADDRESS_LEN     6
#define NAME_LEN     16


struct netdev {
  int mtu;
  uint32_t net_ipaddr; /* network device ip address */
  uint32_t net_mask; /* netmask */
  uint8_t net_hwaddr[ADDRESS_LEN];
  uint8_t net_name[NAME_LEN];
  struct node net_list;
};


struct tapdev {
  struct netdev dev;
  int fd;
};

/* sk_buff linux */
struct sk_buff {
  struct node sk_list;
  uint16_t pro;
  uint16_t type;
  int len;
  int refcnt;
  struct netdev *indev;
  uint8_t data[];
  
}__attribute__((packed));


