#include "ether.h"


struct node netdev_list;


struct netdev *netdev_alloc(char *name)
{
  struct netdev *dev = calloc(1, sizeof(struct netdev));
  node_add_tail(&dev->net_list, &netdev_list);

  dev->net_name[NAME_LEN - 1] = '\0';
  strncpy((char *)dev->net_name, name, NAME_LEN - 1);

  return dev;
}

void netdev_free(struct netdev *dev)
{
  list_remove(&dev->net_list);
  free(dev);
}


void netdev_tx(struct netdev *dev, struct sk_buff skb, int len, uint16_t type, uint8_t *dst)
{

  struct eth_frame *ehdr = (struct ether *)skb->data;

  ehdr->type = htons(type);

  memcpy(ehdr->mac_dst, dst, ETH_ALEN);
  memcpy(ehdr->mac_src, dev->net_hwaddr, ETH_ALEN);

  pkg->len = len + ETH_HEADER_SIZE;

  free_sk_buff(pkg);
}

void netdev_rx(void)
{
  veth_poll();
}
