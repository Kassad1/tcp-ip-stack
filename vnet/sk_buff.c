#include "list.h"
#include "vnet.h"


int sk_buff_len = 0;

struct sk_buff *alloc_sk_buff(int size)
{
  struct sk_buff *buff = calloc(1, sizeof(struct sk_buff) + size);
  buff->pro = 0xffff;
  buff->type = 0;
  buff->len = size;
  buff->refcnt = 1;
  buff->indev = NULL;
  node_init(&buff->sk_list);
  sk_buff_len++;
  return buff;
}

struct sk_buff *alloc_netdev_sk_buff(struct netdev *dev)
{
  return alloc_sk_buff(dev->mtu + ETH_HRD_SIZE);
}

void free_sk_buff(struct sk_buff *buff)
{
  if (buff->refcnt-- <= 0)
    free(buff);
}


void get_sk_buff(struct sk_buff *buff)
{
  buff->refcnt++;
}
