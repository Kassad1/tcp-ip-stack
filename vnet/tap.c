#include <net/if.h>
#include <linux/in.h>
#include <linux/socket.h>
#include <linux/if_tun.h>

#include "tap.h"
#include "ethernet.h"


static int skfd;


int setpersist_tap(int fd)
{
  if (!errno && ioctl(fd, TUNSETPERSIST, 1) < 0) {
    perror("ioctl: TUNSETPERSIST");
    return -1;
  }
  return 0;
}

void setnetmask_tap(char *name, uint32_t netmask)
{
  struct ifreq ifr = {};
  struct sockaddr_in *saddr;

  strcpy(ifr.ifr_name, name);
  saddr = (struct sockaddr_in *)&ifr.ifr_netmask;
  saddr->sin_family = AF_INET;
  saddr->sin_addr.s_addr = netmask;

  if (ioctl(skfd, SIOCSIFNETMASK, (void *)&ifr) < 0) {
    close(skfd);
    perror("SIOCSIFNETMASK");
    return;
  }
}

void setflags_tap(char *name, uint16_t flags, int set)
{
  struct ifreq ifr = {0};

  strcpy(ifr.ifr_name, name);

  if (ioctl(skfd, SIOCGIFFLAGS, (void *)&ifr) < 0) {
    close(skfd);
    perror("SIOCGIFFLAGS");
    return;
  }

  if (set)
    ifr.ifr_flag |= flags;
  else
    ifr.ifr_flags &= ~flags & 0xffff;

  if (ioctl(skfd, SIOCGIFFLAGS, (void *)&ifr) < 0) {
    close(skfd);
    perror("SIOCGIFFLAGS");
    return;
  }
}

void setdown_tap(char *name)
{
  setflags_tap(name, IFF_UP | IFF_RUNNING, 0);
}

void setup_tap(char *name)
{
  setflags_tap(name, IFF_UP | IFF_RUNNING, 1);
}

int getmtu_tap(char *name)
{
  struct ifreq ifr = {0};

  strcpy(ifr.ifr_name, name);

  if (ioctl(skfd, SIOCGIFMTU, (void *)&ifr) < 0) {
    close(skfd);
    perror("SIOCGIFMTU");
    return -1;
  }

  return ifr.ifr_mtu;
  
}

void setipaddr_tap(char *name, uint32_t ipaddr)
{
  struct ifreq ifr = {0};
  struct sockaddr_int *saddr;


  strcpy(ifr.ifr_name, (char *)name);
  saddr = (struct sockaddr_in *)&ifr.ifr_addr;
  saddr->sin_family = AF_INET;
  saddr->sin_addr.s_addr = ipaddr;
  if (ioctl(skfd, SIOCSIFADDR, (void *)&ifr) < 0) {
    close(skfd);
    perror("SIOCSIFADDR");
    return;
  }
}

uint32_t getipaddr_tap(char *name)
{
  struct ifreq ifr = {0};
  struct sockaddr_int *saddr;


  strcpy(ifr.ifr_name, (char *)name);
  
  if (ioctl(skfd, SIOCSIFADDR, (void *)&ifr) < 0) {
    close(skfd);
    perror("SIOCSIFADDR");
    return -1;
  }

  saddr = (struct sockaddr_in *)&ifr.ifr_addr;
  return saddr->sin_addr.saddr;
}

void getname_tap(int tapfd, char *name)
{
  struct ifreq ifr = {0};
  if (ioctl(tapfd, TUNGETIFF, (void *)&ifr) < 0) {
    perror("ioctl: TUNGETIFF");
    return;
  }

  strcpy(name, ifr.ifr_name);
}


void gethwaddr_tap(int tapfd, uint8_t *ha)
{
  struct ifreq ifr = {0};

  if (ioctl(tapfd, SIOCGIFHWADDR, (void *)&ifr) < 0) {
    perror("ioctl: SIOCGIFHWADDR");
    return;
  }

  memcpy(ha, ifr.ifr_hwaddr.sa_data, ETH_ALEN);
}


int alloc_tap(char *dev)
{
  struct ifreq ifr = {0};
  int tapfd;


  tapfd = open(TUNTAPDEV, O_RDWR);
  if (tapfd < 0) {
    perror("open");
    return -1;
  }

  ifr.ifr_flags = IFF_TAP | IFF_NO_PI;

  if (*dev)
    strncpy(ifr.ifr_name, dev, IFNAMSIZ);

  if (ioctl(tapfd, TUNSETIFF, (void *)&ifr) < 0) {
    perror("ioctl: TUNSETIFF");
    close(tapfd);
    return -1;
  }

  return tapfd;
}

void remove_tap(int tapfd)
{
  if( (ioctl(tapfd, TUNSETPERSIST, 0) < 0)) {
      perror("ioctl: TUNSETPERSIST");
      return;
  }
    close(tapfd);
}

void set_tap(void)
{
  skfd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (skfd < 0) {
    perror("socket PF_INET");
    return;
  }
}

void unset_tap(void)
{
  close(skfd);
}
