#include "tap.h"
#include "list.h"
#include "vnet.h"

struct tapdev *tap;
struct netdev *veth;


int tap_dev_init(void)
{

  tap = malloc(sizeof(struct tapdev));
  tap->fd = alloc_tap("tap0");

  if (tap->fd < 0) {
    free(tap);
    return -1;
  }

  if (setpersist_tap(tap->fd) < 0)
    close(tap->fd);

  set_tap();
  getname_tap(tap->fd, tap->dev.net_name);
  getmtu_tap(tap->dev.net_name, &tap->dev.net_mtu);
  /******************************************/
  gethwaddr_tap(tap->fd, tap->dev.net_hwaddr);
  setipaddr_tap(tap->dev.net_name, "     ");
  tap->dev.net_ipaddr = getipaddr_tap(tap->dev.net_name);
  setnetmask_tap(tap->dev.net_name, "     ");
  setup_tap(tap->dev.net_name);
  /******************************************/

  unset_tap();

  node_init(&tap->dev.net_list);
  return 0;
}

void veth_dev_remove(struct netdev *dev)
{
  if (dev != veth)
    perror("veth_dev_remove(): Device Error");
  delete_tap(tap->fd);
}

int veth_dev_init(struct netdev *dev)
{
  if (tap_dev_init() < 0)
    perror("TAP Device Error");

  dev->net_mtu = tap->dev.net_mtu;
  dev->net_ipaddr = "     ";
  dev->net_mask = "      ";

  memcpy(dev->net_hwaddr, "     ");

  return 0;
}


void veth_tx(struct netdev *dev, struct sk_buff *buff)
{
  
  if(write(tap->fd, buff->data, buff->len) < 0)
    perror("veth_tx: write()");
}

void veth_rx(void)
{
  struct sk_buff *buff = alloc_netdev_sk_buff(veth);

  if (read(tap->fd, buff->data, buff->len) > 0)
    parser_net(veth, pkb);
  else
    free_sk_buff(buff);
}

void veth_poll(void)
{
  struct pollff pfd = {0);
  int ret;
  while(1) {
    pfd.fd = tap->fd;
    pfd.events = POLLIN;
    pfd.revents = 0;

    ret = poll(&pfd, 1, -1);

    if (ret <= 0) {
      perror("poll failed");
      exit(EXIT_FAILURE);
  }

    veth_rx();
}


void veth_init(void)
{
  veth = netdev_alloc("veth");
}
